package com.weather.app.controllers;

import com.weather.app.models.WeatherEntity;
import com.weather.app.services.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * This is weather entity controller
 */
@RestController
@CrossOrigin
@RequestMapping(path = "/v1/open_weather")
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    /**
     * This API is for getting information from open weather website
     *
     * @param city is city of weather entity
     * @return
     */
    @PostMapping("/weather")
    @ResponseBody
    public WeatherEntity addWeather(@RequestParam("city") String city) {
        return weatherService.addWeather(city);
    }

    /**
     * This API returns a list of weather entity
     *
     * @return
     */
    @GetMapping("/weather")
    @ResponseBody
    public Iterable<WeatherEntity> getWeatherList() {
        return weatherService.getWeatherList();
    }

    /**
     * This API is for getting specific weather entity by id
     *
     * @param id is id of weather entity
     * @return
     */
    @GetMapping("/weather/{id}")
    @ResponseBody
    public WeatherEntity getWeatherById(@PathVariable(required = true) Integer id) {
        return weatherService.getWeatherById(id);
    }

    /**
     * This API is for getting weather entity list by city
     *
     * @param city is city of weather entity
     * @return
     */
    @GetMapping("/weather/city/{city}")
    @ResponseBody
    public Iterable<WeatherEntity> getWeatherListByCity(@PathVariable(required = true) String city) {
        return weatherService.getWeatherListByCity(city);
    }

    /**
     * This API is for getting weather entity list by country
     *
     * @param country is country of weather entity
     * @return
     */
    @GetMapping("/weather/country/{country}")
    @ResponseBody
    public Iterable<WeatherEntity> getWeatherListByCountry(@PathVariable(required = true) String country) {
        return weatherService.getWeatherListByCountry(country);
    }

}
