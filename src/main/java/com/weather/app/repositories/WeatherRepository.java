package com.weather.app.repositories;

import com.weather.app.models.WeatherEntity;
import org.springframework.data.repository.CrudRepository;

public interface WeatherRepository extends CrudRepository<WeatherEntity, Integer> {

    Iterable<WeatherEntity> findByCity(String city);

    Iterable<WeatherEntity> findByCountry(String country);

}
