package com.weather.app.utils;

public class Constants {

    public static final String APP_ID = "b5766cb4e4f9dd898aa37a4da54f2ec9";

    public static final String WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={appid}";

}
