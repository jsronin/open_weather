package com.weather.app.services;

import com.weather.app.dtos.WeatherResponse;
import com.weather.app.models.WeatherEntity;
import com.weather.app.repositories.WeatherRepository;
import com.weather.app.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.weather.app.mappers.WeatherMapper.toEntity;

/**
 * This is an implementation of weather service
 */
@Service
public class WeatherServiceImpl implements WeatherService {

    @Autowired
    WeatherRepository weatherRepository;
    @Autowired
    private RestTemplate restTemplate;

    /**
     * This method is for getting information from open weather website
     *
     * @param city is city of weather entity
     * @return
     */
    @Override
    public WeatherEntity addWeather(String city) {
        String url = Constants.WEATHER_API_URL.replace("{city}", city).replace("{appid}", Constants.APP_ID);
        ResponseEntity<WeatherResponse> response = restTemplate.getForEntity(url, WeatherResponse.class);
        WeatherEntity weatherEntity = toEntity(response.getBody());
        return weatherRepository.save(weatherEntity);
    }

    /**
     * This method is for getting specific weather entity by id
     *
     * @param id is id of weather entity
     * @return
     */
    @Override
    public WeatherEntity getWeatherById(Integer id) {
        return weatherRepository.findById(id).orElse(null);
    }

    /**
     * This method returns a list of weather entity
     *
     * @return
     */
    @Override
    public Iterable<WeatherEntity> getWeatherList() {
        return weatherRepository.findAll();
    }

    /**
     * This method is for getting weather entity list by city
     *
     * @param city is city of weather entity
     * @return
     */
    @Override
    public Iterable<WeatherEntity> getWeatherListByCity(String city) {
        return weatherRepository.findByCity(city);
    }

    /**
     * This method is for getting weather entity list by country
     *
     * @param country is country of weather entity
     * @return
     */
    @Override
    public Iterable<WeatherEntity> getWeatherListByCountry(String country) {
        return weatherRepository.findByCountry(country);
    }

}
