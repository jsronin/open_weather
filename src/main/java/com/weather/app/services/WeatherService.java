package com.weather.app.services;

import com.weather.app.models.WeatherEntity;

public interface WeatherService {

    WeatherEntity addWeather(String city);

    WeatherEntity getWeatherById(Integer id);

    Iterable<WeatherEntity> getWeatherList();

    Iterable<WeatherEntity> getWeatherListByCity(String city);

    Iterable<WeatherEntity> getWeatherListByCountry(String country);

}
