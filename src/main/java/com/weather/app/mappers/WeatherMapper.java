package com.weather.app.mappers;

import com.weather.app.dtos.WeatherResponse;
import com.weather.app.models.WeatherEntity;

import java.time.LocalDateTime;

/**
 * This is a mapper class for mapping response dto to entity
 */
public class WeatherMapper {

    /**
     * This is to entity mapper
     *
     * @param response is response dto
     * @return
     */
    public static WeatherEntity toEntity(WeatherResponse response) {
        WeatherEntity entity = new WeatherEntity();
        entity.setDateTime(LocalDateTime.now());
        entity.setCondition(response.getWeather().get(0).getMain());
        entity.setDescription(response.getWeather().get(0).getDescription());
        entity.setLongitude(response.getCoord().getLon());
        entity.setLatitude(response.getCoord().getLat());
        entity.setCity(response.getName());
        entity.setCountry(response.getSys().getCountry());
        entity.setTemperature(response.getMain().getTemp());
        entity.setMinTemperature(response.getMain().getTempMin());
        entity.setMaxTemperature(response.getMain().getTempMax());
        entity.setPressure(response.getMain().getPressure());
        entity.setHumidity(response.getMain().getHumidity());
        entity.setVisibility(response.getVisibility());
        entity.setWindSpeed(response.getWind().getSpeed());
        entity.setWindDegree(response.getWind().getDeg());
        entity.setClouds(response.getClouds().getAll());
        return entity;
    }


}
